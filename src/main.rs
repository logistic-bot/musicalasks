// Copyright 2024 Khaïs COLIN <khais.colin@gmail.com>. All rights reserved.
//
// This file is part of Musicalasks.
//
// Musicalasks is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
//
// Musicalasks is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Musicalasks. If not, see <https://www.gnu.org/licenses/>. 
use std::{path::PathBuf, time::Duration};

use clap::Parser;
use color_eyre::eyre::Result;
use musicalasks::update::update;
use ratatui::{backend::CrosstermBackend, Terminal};

use musicalasks::app::App;
use musicalasks::event::EventHandler;
use musicalasks::tui::Tui;

/// TUI audio tags editor, similar to puddletag
#[derive(Parser, Debug)]
#[command(version, about)]
struct Args {
    /// Path to directories with audio files
    ///
    /// Will be scanned recursively.
    paths: Vec<PathBuf>,
}

fn main() -> Result<()> {
    // parse args
    let args = Args::parse();

    // create app
    let mut app = App::new(args.paths)?;

    // setup tui
    let backend = CrosstermBackend::new(std::io::stdout());
    let terminal = Terminal::new(backend)?;
    let events = EventHandler::new(Duration::from_millis(250));
    let mut tui = Tui::new(terminal, events);
    tui.enter()?;

    // mail loop
    while !app.should_quit {
        // Render
        tui.draw(&mut app)?;

        // handle events
        let event = tui.events.next()?;
        update(&mut app, event);
    }

    // exit
    tui.exit()?;
    Ok(())
}
