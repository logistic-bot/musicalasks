// Copyright 2024 Khaïs COLIN <khais.colin@gmail.com>. All rights reserved.
//
// This file is part of Musicalasks.
//
// Musicalasks is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
//
// Musicalasks is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Musicalasks. If not, see <https://www.gnu.org/licenses/>. 
/// Application
pub mod app;
/// Terminal Event Handler
pub mod event;
/// Terminal User Interface
pub mod tui;
/// Widget renderer
pub mod ui;
/// Application updater
pub mod update;
