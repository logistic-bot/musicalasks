// Copyright 2024 Khaïs COLIN <khais.colin@gmail.com>. All rights reserved.
//
// This file is part of Musicalasks.
//
// Musicalasks is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
//
// Musicalasks is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Musicalasks. If not, see <https://www.gnu.org/licenses/>. 
use std::io;

use color_eyre::Result;
use crossterm::{
    event::{DisableMouseCapture, EnableMouseCapture},
    terminal::{EnterAlternateScreen, LeaveAlternateScreen},
};

use crate::{app::App, event::EventHandler, ui};

pub type CrosstermTerminal = ratatui::Terminal<ratatui::backend::CrosstermBackend<std::io::Stdout>>;

/// Representation of a terminal user interface.
///
/// It is responsible for setting up the terminal,
/// initalizing the interface and handling the draw events.
pub struct Tui {
    /// Interface to the terminal
    terminal: CrosstermTerminal,
    /// Terminal event handler
    pub events: EventHandler,
}

impl Tui {
    /// Constructs a new instance of [`Tui`]
    pub fn new(terminal: CrosstermTerminal, events: EventHandler) -> Self {
        Self { terminal, events }
    }

    /// Initializes the terminal interface.
    ///
    /// It enables the raw mode and sets terminal properties.
    pub fn enter(&mut self) -> Result<()> {
        crossterm::terminal::enable_raw_mode()?;
        crossterm::execute!(io::stdout(), EnterAlternateScreen, EnableMouseCapture,)?;

        // Define panic hook
        let hook_builder = color_eyre::config::HookBuilder::default();
        let (panic_hook, eyre_hook) = hook_builder.into_hooks();

        // convert from a color_eyre PanicHook to a standard panic hook
        let panic_hook = panic_hook.into_panic_hook();
        std::panic::set_hook(Box::new(move |panic_info| {
            Self::reset().expect("failed to reset the terminal");
            panic_hook(panic_info);
        }));

        // convert from a color_eyre EyreHook to a eyre ErrorHook
        let eyre_hook = eyre_hook.into_eyre_hook();
        color_eyre::eyre::set_hook(Box::new(move |error| {
            Self::reset().expect("failed to reset the terminal");
            eyre_hook(error)
        }))?;

        self.terminal.hide_cursor()?;
        self.terminal.clear()?;

        Ok(())
    }

    /// Resets the terminal interface.
    ///
    /// This function is also used for the panic hook to revert the terminal
    /// properties if unexpected errors occur.
    fn reset() -> Result<()> {
        crossterm::terminal::disable_raw_mode()?;
        crossterm::execute!(io::stdout(), LeaveAlternateScreen, DisableMouseCapture)?;
        Ok(())
    }

    /// Exits the terminal interface.
    ///
    /// It disables the raw mode and reverts back the terminal properties.
    pub fn exit(&mut self) -> Result<()> {
        Self::reset()?;
        self.terminal.show_cursor()?;
        Ok(())
    }

    /// [`Draw`] the terminal interface by [`rendering`] the widgets.
    ///
    /// [`Draw`]: tui::Terminal::draw
    /// [`rendering`]: crate::ui::render
    pub fn draw(&mut self, app: &mut App) -> Result<()> {
        self.terminal.draw(|f| ui::render(app, f))?;
        Ok(())
    }
}
