// Copyright 2024 Khaïs COLIN <khais.colin@gmail.com>. All rights reserved.
//
// This file is part of Musicalasks.
//
// Musicalasks is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
//
// Musicalasks is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Musicalasks. If not, see <https://www.gnu.org/licenses/>. 
use ratatui::{prelude::*, widgets::Table};

use crate::app::{track::Track, App};

pub fn render(app: &mut App, f: &mut ratatui::prelude::Frame<'_>) {
    let layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(vec![
            Constraint::Fill(100),
            Constraint::Length(1),
            Constraint::Length(1),
            Constraint::Length(1),
        ])
        .split(f.size());

    let mut tracks_state = app.tracks.state.clone();
    let rows = app.tracks.to_table_rows();
    let widths = [
        Constraint::Length(25),
        Constraint::Fill(25),
        Constraint::Fill(12),
        Constraint::Length(5),
        Constraint::Length(1),
        Constraint::Fill(12),
        Constraint::Fill(12),
        Constraint::Max(4),
        Constraint::Fill(8),
    ];
    let table = Table::new(rows, widths)
        .header(Track::table_header())
        .highlight_style(Style::new().on_light_green());
    f.render_stateful_widget(table, layout[0], &mut tracks_state);
    app.tracks.state = tracks_state;

    let pathline = Text::from(
        app.tracks
            .selected()
            .map(|x| x.path.display().to_string())
            .unwrap_or_default(),
    );
    f.render_widget(pathline, layout[1]);

    let status = Text::from(format!(
        "[{}] {} tracks{}{}",
        app.current_mode(),
        app.tracks.items.len(),
        if app.tracks_scan_finished {
            ""
        } else {
            " loading..."
        },
        app.tracks
            .selected()
            .map(|x| format!(
                " | {} - {}",
                x.tags.artist().unwrap_or_default(),
                x.tags.title().unwrap_or_default()
            ))
            .unwrap_or_default()
    ))
    .style(app.status_bar_style());
    f.render_widget(status, layout[2]);

    let prefix_layout = Layout::default()
        .direction(Direction::Horizontal)
        .constraints(vec![Constraint::Length(1), Constraint::Fill(100)])
        .split(layout[3]);

    match app.current_mode() {
        crate::app::Mode::Navigation
        | crate::app::Mode::WritingTags
        | crate::app::Mode::TagSelect(_) => {
            f.render_widget(Text::from(app.status.as_str()), layout[3]);
        }
        crate::app::Mode::Command => {
            f.render_widget(Text::from(":"), prefix_layout[0]);
            f.render_widget(app.commandbox_widget(), prefix_layout[1])
        }
        crate::app::Mode::SearchEdit => {
            f.render_widget(Text::from("/"), prefix_layout[0]);
            f.render_widget(app.searchbox_widget(), prefix_layout[1])
        }
        crate::app::Mode::TitleEdit => f.render_widget(app.tagedit_widget(), layout[3]),
        crate::app::Mode::TagEdit(_) => f.render_widget(app.tagedit_widget(), layout[3]),
    }
}
