// Copyright 2024 Khaïs COLIN <khais.colin@gmail.com>. All rights reserved.
//
// This file is part of Musicalasks.
//
// Musicalasks is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
//
// Musicalasks is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Musicalasks. If not, see <https://www.gnu.org/licenses/>. 
use std::num::ParseIntError;
use std::path::Path;
use std::path::PathBuf;

use ratatui::style::Style;
use ratatui::style::Stylize;
use ratatui::text::Text;
use ratatui::widgets::Cell;
use ratatui::widgets::Row;

use super::tag::Tag;

fn selected_style() -> Style {
    Style::new().blue().bold().italic()
}

type AudioTag = Box<dyn audiotags::AudioTag + Send + Sync>;

pub struct Track {
    /// Path to the audio file
    pub path: PathBuf,
    /// Tags for this track
    pub tags: AudioTag,
    /// Set to true if tags have been modified since last write call.
    pub changed: bool,
    /// Selected tag, if any to be bulk-edited
    pub selected_tag: Option<Tag>,
}

impl Track {
    pub fn from_path<P>(path: P) -> audiotags::Result<Self>
    where
        P: AsRef<Path>,
    {
        let tags = audiotags::Tag::new().read_from_path(&path)?;
        Ok(Self {
            path: path.as_ref().to_path_buf(),
            tags,
            changed: false,
            selected_tag: None,
        })
    }

    fn path(&self) -> Cell {
        let text = self.path.file_name().unwrap_or_default().to_string_lossy();
        let last_25 = {
            let split_pos = text.char_indices().nth_back(24).unwrap_or((0, ' ')).0;
            &text[split_pos..]
        }
        .to_string();
        let content = Text::from(last_25).dim();
        Cell::new(content)
    }

    fn title(&self) -> Cell {
        Cell::new(self.tags.title().unwrap_or_default())
    }

    fn artist(&self) -> Cell {
        Cell::new(self.tags.artist().unwrap_or_default())
    }

    fn track_number(&self) -> Cell {
        let content = format!(
            "{}/{}",
            self.tags.track_number().unwrap_or_default(),
            self.tags.total_tracks().unwrap_or_default()
        );
        let content = Text::from(content).right_aligned();
        Cell::new(content)
    }

    fn disk_number(&self) -> Cell {
        Cell::new(self.tags.disc_number().unwrap_or_default().to_string())
    }

    fn album_title(&self) -> Cell {
        Cell::new(self.tags.album_title().unwrap_or_default()).style(
            if self.selected_tag == Some(Tag::Album) {
                selected_style()
            } else {
                Style::default()
            },
        )
    }

    fn album_artist(&self) -> Cell {
        Cell::new(self.tags.album_artist().unwrap_or_default())
    }

    fn year(&self) -> Cell {
        Cell::new(self.tags.year().unwrap_or_default().to_string())
    }

    fn genre(&self) -> Cell {
        Cell::new(self.tags.genre().unwrap_or_default())
    }

    pub fn to_table_row(&self) -> Row<'_> {
        let cells = vec![
            self.path(),
            self.title(),
            self.artist(),
            self.track_number(),
            self.disk_number(),
            self.album_title(),
            self.album_artist(),
            self.year(),
            self.genre(),
        ];
        Row::new(cells)
    }

    pub fn table_header() -> Row<'static> {
        let cells = vec![
            "Path".bold(),
            "Title".bold(),
            "Artist".bold(),
            "Track".bold(),
            "Disk".bold(),
            "Album".bold(),
            "AArtist".bold(),
            "Year".bold(),
            "Genre".bold(),
        ];
        Row::new(cells)
    }

    pub fn search_matches(&self, needle: &str) -> bool {
        let haystack = format!(
            "{} {} {} {} {} {} {} {} {}",
            self.tags.title().unwrap_or_default(),
            self.tags.artist().unwrap_or_default(),
            self.tags.album_title().unwrap_or_default(),
            self.tags.album_artist().unwrap_or_default(),
            self.tags.year().map(|x| x.to_string()).unwrap_or_default(),
            self.tags.genre().unwrap_or_default(),
            self.path.to_string_lossy(),
            self.tags
                .track_number()
                .map(|x| x.to_string())
                .unwrap_or_default(),
            self.tags
                .disc_number()
                .map(|x| x.to_string())
                .unwrap_or_default(),
        )
        .to_lowercase();
        for word in needle.to_lowercase().split_ascii_whitespace() {
            if !haystack.contains(word) {
                return false;
            }
        }
        true
    }

    pub fn write(&mut self) -> color_eyre::Result<()> {
        let path: Option<&str> = self.path.to_str();
        let path = path.expect(
            "Due to a limitation in the audiotags library, only UTF-8 paths are supported.",
        );
        self.tags.write_to_path(path)?;
        self.changed = false;
        Ok(())
    }

    pub fn set_title(&mut self, new_title: &str) {
        self.tags.set_title(new_title);
        self.changed = true;
    }

    /// Get the value of the given tag for this track. If no value is currently set, return an empty string.
    ///
    /// Number tags are converted to strings.
    pub fn get_tag_value(&self, tag: Tag) -> String {
        match tag {
            Tag::Title => self.tags.title().unwrap_or_default().to_string(),
            Tag::Artist => self.tags.artist().unwrap_or_default().to_string(),
            Tag::TrackNumber => self
                .tags
                .track_number()
                .map(|x| x.to_string())
                .unwrap_or_default(),
            Tag::DiscNumber => self
                .tags
                .disc_number()
                .map(|x| x.to_string())
                .unwrap_or_default(),
            Tag::Album => self.tags.album_title().unwrap_or_default().to_string(),
            Tag::AlbumArtist => self.tags.album_artist().unwrap_or_default().to_string(),
            Tag::Year => self
                .tags
                .year()
                .map(|x| x.to_string())
                .unwrap_or_default()
                .to_string(),
            Tag::Genre => self.tags.genre().unwrap_or_default().to_string(),
        }
    }

    /// Set the given tag to the new value.
    ///
    /// Will automatically convert strings to numbers for track number, disc
    /// number and year tag, returning the `ParseIntError` if needed
    pub fn set_tag_value(&mut self, tag: Tag, new_value: &str) -> Result<(), ParseIntError> {
        match tag {
            Tag::Title => self.tags.set_title(new_value),
            Tag::Artist => self.tags.set_artist(new_value),
            Tag::TrackNumber => self.tags.set_track_number(new_value.parse()?),
            Tag::DiscNumber => self.tags.set_disc_number(new_value.parse()?),
            Tag::Album => self.tags.set_album_title(new_value),
            Tag::AlbumArtist => self.tags.set_album_artist(new_value),
            Tag::Year => self.tags.set_year(new_value.parse()?),
            Tag::Genre => self.tags.set_genre(new_value),
        }
        Ok(())
    }
}

impl std::fmt::Display for Track {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Filename: {}", self.path.display())?;
        writeln!(f, "Artist: {}", self.tags.artist().unwrap_or_default())?;
        writeln!(f, "Title: {}", self.tags.title().unwrap_or_default())?;
        writeln!(
            f,
            "Track: {}/{}",
            self.tags.track_number().unwrap_or_default(),
            self.tags.total_tracks().unwrap_or_default()
        )?;
        writeln!(
            f,
            "Disknumber: {}",
            self.tags.disc_number().unwrap_or_default()
        )?;
        writeln!(f, "Album: {}", self.tags.album_title().unwrap_or_default())?;
        writeln!(f, "Year: {}", self.tags.year().unwrap_or_default())?;
        writeln!(f, "Genre: {}", self.tags.genre().unwrap_or_default())?;
        writeln!(f, "Comment: {}", self.tags.comment().unwrap_or_default())?;
        writeln!(
            f,
            "Album Artist: {}",
            self.tags.album_artist().unwrap_or_default()
        )
    }
}
