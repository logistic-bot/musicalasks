// Copyright 2024 Khaïs COLIN <khais.colin@gmail.com>. All rights reserved.
//
// This file is part of Musicalasks.
//
// Musicalasks is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
//
// Musicalasks is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Musicalasks. If not, see <https://www.gnu.org/licenses/>.
use ratatui::widgets::{Row, TableState};

use super::track::Track;

pub struct Tracks {
    pub items: Vec<Track>,
    pub state: TableState,
}

impl Tracks {
    pub fn new(tracks: Vec<Track>) -> Self {
        Self {
            items: tracks,
            state: TableState::default(),
        }
    }

    pub fn to_table_rows(&self) -> Vec<Row> {
        let mut rows = Vec::with_capacity(self.items.len());

        for track in &self.items {
            let row = track.to_table_row();
            rows.push(row);
        }

        rows
    }

    // Select the next item. This will not be reflected until the widget is drawn in the
    // `Terminal::draw` callback using `Frame::render_stateful_widget`.
    pub fn next(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i >= self.items.len() - 1 {
                    0
                } else {
                    i + 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    // Select the previous item. This will not be reflected until the widget is drawn in the
    // `Terminal::draw` callback using `Frame::render_stateful_widget`.
    pub fn previous(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i == 0 {
                    self.items.len() - 1
                } else {
                    i - 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    pub fn select_first(&mut self) {
        self.state.select(Some(0))
    }

    pub fn select_last(&mut self) {
        self.state.select(Some(self.items.len() - 1))
    }

    pub fn first(&self) -> Option<&Track> {
        self.items.first()
    }

    pub fn pageup(&mut self) {
        let i = self.state.selected().unwrap_or(self.items.len() - 1);
        self.state.select(Some(i.saturating_sub(5)))
    }

    pub fn pagedown(&mut self) {
        let i = self.state.selected().unwrap_or(0);
        self.state
            .select(Some(std::cmp::min(self.items.len() - 1, i + 5)))
    }

    pub fn selected(&self) -> Option<&Track> {
        self.state.selected().and_then(|x| self.items.get(x))
    }

    pub fn selected_mut(&mut self) -> Option<&mut Track> {
        self.state.selected().and_then(|x| self.items.get_mut(x))
    }

    pub fn sort(&mut self) {
        self.items.sort_by(|a, b| {
            a.tags
                .album_artist()
                .cmp(&b.tags.album_artist())
                .then(a.tags.year().cmp(&b.tags.year()))
                .then(a.tags.album_title().cmp(&b.tags.album_title()))
                .then(a.tags.disc_number().cmp(&b.tags.disc_number()))
                .then(a.tags.track_number().cmp(&b.tags.track_number()))
                .then(a.tags.title().cmp(&b.tags.title()))
                .then(a.path.cmp(&b.path))
        })
    }

    /// Write all tag changes to file
    ///
    /// Returns the number of tracks that were saved.
    pub fn write(&mut self) -> color_eyre::Result<usize> {
        let mut written_tracks = 0;
        for track in &mut self.items {
            if track.changed {
                track.write()?;
                written_tracks += 1;
            }
        }
        Ok(written_tracks)
    }
}
