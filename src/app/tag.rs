// Copyright 2024 Khaïs COLIN <khais.colin@gmail.com>. All rights reserved.
//
// This file is part of Musicalasks.
//
// Musicalasks is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
//
// Musicalasks is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Musicalasks. If not, see <https://www.gnu.org/licenses/>. 
#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum Tag {
    Title,
    Artist,
    TrackNumber,
    DiscNumber,
    Album,
    AlbumArtist,
    Year,
    Genre,
}

impl std::fmt::Display for Tag {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Tag::Title => "title",
                Tag::Artist => "artist",
                Tag::TrackNumber => "track number",
                Tag::DiscNumber => "disc number",
                Tag::Album => "album",
                Tag::AlbumArtist => "album artist",
                Tag::Year => "year",
                Tag::Genre => "genre",
            }
        )
    }
}
