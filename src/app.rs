// Copyright 2024 Khaïs COLIN <khais.colin@gmail.com>. All rights reserved.
//
// This file is part of Musicalasks.
//
// Musicalasks is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
//
// Musicalasks is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Musicalasks. If not, see <https://www.gnu.org/licenses/>. 
use std::{
    collections::VecDeque,
    path::PathBuf,
    sync::mpsc::{self, Receiver},
    thread,
};

use self::{tag::Tag, track::Track, tracks::Tracks};
use color_eyre::Result;
use ratatui::style::{Style, Stylize};
use tui_textarea::TextArea;
use walkdir::WalkDir;

/// The concept of each individual tag
pub mod tag;
/// A single music track/file
pub mod track;
/// A list of tracks
pub mod tracks;

pub struct App<'a> {
    /// List of tracks that can be edited.
    ///
    /// Order of this list is the same as the display order in the UI.
    pub tracks: Tracks,
    /// Set to true if the user has requested to quit, and the application should exit.
    pub should_quit: bool,

    /// Tracks received from this channel will be added to the list
    tracks_rx: Receiver<Track>,

    /// Set to true when all tracks have been scanned.
    pub tracks_scan_finished: bool,

    /// Curent mode is the last element in the mode stack
    mode_stack: VecDeque<Mode>,

    /// Text editing area for search
    searchbox: TextArea<'a>,

    /// Current status message
    pub status: String,

    /// Edit box for tags
    tagedit: TextArea<'a>,

    /// Edit box for commands
    commandbox: TextArea<'a>,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Mode {
    /// No editing, navigate in the list trackwise
    Navigation,
    /// Edit a command to be executed.
    ///
    /// Stuff like write, quit, etc.
    Command,
    /// Currently writing tags to file. No editing allowed!
    WritingTags,
    /// Edit search term with live search
    SearchEdit,
    /// Edit the title of the selected track
    TitleEdit,
    /// Selecting tracks to bulk-edit a specific tag
    TagSelect(Tag),
    /// Editing the value of a single tag for multiple tracks at the same time
    TagEdit(Tag),
}

impl std::fmt::Display for Mode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Mode::Navigation => write!(f, "NAV"),
            Mode::Command => write!(f, "CMD"),
            Mode::WritingTags => write!(f, "WRT"),
            Mode::SearchEdit => write!(f, "SRH"),
            Mode::TitleEdit => write!(f, "ETT"),
            Mode::TagSelect(tag) => write!(f, "TSL({})", tag),
            Mode::TagEdit(tag) => write!(f, "TED({})", tag),
        }
    }
}

impl App<'_> {
    pub fn new<P>(paths: Vec<P>) -> Result<Self>
    where
        P: Into<PathBuf>,
    {
        let (tracks_tx, tracks_rx) = mpsc::channel();
        let paths: Vec<PathBuf> = paths.into_iter().map(|x| x.into()).collect();
        thread::spawn(move || {
            for path in &paths {
                WalkDir::new(path).into_iter().for_each(|entry| {
                    if let Ok(entry) = entry {
                        if !entry
                            .metadata()
                            .expect("failed to get entry metadata")
                            .is_dir()
                        {
                            let maybe_track = Track::from_path(entry.path());
                            if let Ok(track) = maybe_track {
                                tracks_tx
                                    .send(track)
                                    .expect("failed to send new track to other thread");
                            };
                        }
                    }
                });
            }
        });

        Ok(Self {
            tracks: Tracks::new(Vec::new()),
            should_quit: false,
            tracks_rx,
            tracks_scan_finished: false,
            mode_stack: VecDeque::new(),
            searchbox: Self::create_textarea(),
            status: String::new(),
            tagedit: Self::create_textarea(),
            commandbox: Self::create_textarea(),
        })
    }

    pub fn quit(&mut self) {
        self.should_quit = true;
    }

    fn recieve_new_tracks(&mut self) {
        loop {
            let maybe_track = self.tracks_rx.try_recv();
            match maybe_track {
                Ok(track) => self.tracks.items.push(track),
                Err(e) => match e {
                    mpsc::TryRecvError::Empty => break,
                    mpsc::TryRecvError::Disconnected => {
                        self.tracks_scan_finished = true;
                        break;
                    }
                },
            }
        }

        self.tracks.sort();
    }

    pub fn current_mode(&self) -> &Mode {
        self.mode_stack.back().unwrap_or(&Mode::Navigation)
    }

    pub fn update(&mut self) {
        use Mode::*;

        self.recieve_new_tracks();

        match self.current_mode() {
            Navigation | Command | SearchEdit | TitleEdit | TagSelect(_) | TagEdit(_) => {}
            WritingTags => match self.write_tags_of_next_track() {
                WritingStatus::InProgress => {}
                WritingStatus::Finished => self.previous_mode(),
            },
        }
    }

    pub fn navigate(&mut self) {
        self.mode_stack.push_back(Mode::Navigation);
        self.search_next();
    }

    pub fn search_edit(&mut self) {
        self.mode_stack.push_back(Mode::SearchEdit);
        self.searchbox = Self::create_textarea();
    }

    pub fn command_edit(&mut self) {
        self.mode_stack.push_back(Mode::Command);
        self.commandbox = Self::create_textarea();
    }

    /// Write all tag changes to file.
    pub fn write(&mut self) {
        self.mode_stack.push_back(Mode::WritingTags);
    }

    /// Select the 'album' tag of the current track for editing.
    ///
    /// Also switch to TagSelect(Tag::Album) mode.
    pub fn album_tag_select(&mut self) {
        if !matches!(self.current_mode(), Mode::TagSelect(Tag::Album)) {
            self.mode_stack.push_back(Mode::TagSelect(Tag::Album));
        }
        if let Some(track) = &mut self.tracks.selected_mut() {
            if track.selected_tag == Some(Tag::Album) {
                track.selected_tag = None;
            } else {
                track.selected_tag = Some(Tag::Album);
            }
            self.tracks.next();
        }
    }

    /// Once the user has selected some tags, this initiate the edit of the tag's value.
    pub fn tag_edit_start(&mut self) {
        if let Mode::TagSelect(tag) = self.current_mode() {
            self.mode_stack.push_back(Mode::TagEdit(*tag));

            let current_tag_value = self.get_current_selected_tag_value().unwrap_or_default();

            self.tagedit = Self::create_textarea();
            self.tagedit.insert_str(current_tag_value);
        }
    }

    pub fn search_next(&mut self) {
        self.search_select_match(SearchDirection::Forwards)
    }

    pub fn search_previous(&mut self) {
        self.search_select_match(SearchDirection::Backwards)
    }

    pub fn search_select_match(&mut self, direction: SearchDirection) {
        if self.tracks.items.is_empty() {
            return;
        }
        let mut current_index = self
            .tracks
            .state
            .selected()
            .map(|x| match direction {
                SearchDirection::Forwards => x + 1,
                SearchDirection::Backwards => x - 1,
            })
            .unwrap_or(0);
        let needle = self.searchbox();
        if needle.is_empty() {
            return;
        }
        let mut reached_end_once = false;
        let mut limit = self.tracks.items.len() * 3;
        while limit > 0 {
            limit -= 1;
            if let Some(track) = self.tracks.items.get(current_index) {
                if track.search_matches(&needle) {
                    self.tracks.state.select(Some(current_index));
                    break;
                } else {
                    match direction {
                        SearchDirection::Forwards => current_index += 1,
                        SearchDirection::Backwards => current_index -= 1,
                    }
                }
            } else {
                // we reached the end of the track list
                if !reached_end_once {
                    reached_end_once = true;
                    current_index = match direction {
                        SearchDirection::Forwards => 0,
                        SearchDirection::Backwards => self.tracks.items.len() - 1,
                    };
                } else {
                    self.status = "No results".to_string();
                    break;
                }
            }
        }
    }

    fn create_textarea<'a>() -> TextArea<'a> {
        let mut searchbox = TextArea::default();
        searchbox.set_cursor_line_style(Style::default());
        searchbox
    }

    pub fn title_edit(&mut self) {
        if let Some(track) = self.tracks.selected() {
            self.mode_stack.push_back(Mode::TitleEdit);
            let mut tagedit = Self::create_textarea();
            tagedit.insert_str(track.tags.title().unwrap_or_default());
            self.tagedit = tagedit;
        }
    }

    /// Commit the tag edit that is currently in progress, with the user typing in the new value
    pub fn commit_tag_edit(&mut self) {
        let current_mode = self.current_mode().clone();
        match current_mode {
            Mode::Navigation
            | Mode::SearchEdit
            | Mode::Command
            | Mode::WritingTags
            | Mode::TagSelect(_) => (),
            Mode::TitleEdit => {
                let new_title = self.tagedit();
                if let Some(track) = self.tracks.selected_mut() {
                    track.set_title(&new_title);
                }
                self.tracks.next();
                self.previous_mode();
            }
            Mode::TagEdit(tag) => {
                let new_value = self.tagedit();
                let err = self
                    .tracks
                    .items
                    .iter_mut()
                    .filter(|track| track.selected_tag == Some(tag))
                    .filter_map(|track| track.set_tag_value(tag, &new_value).err())
                    .next();
                if let Some(err) = err {
                    self.status = format!(
                        "Error while commiting tag edit, possibly a malformed number: {}",
                        err
                    );
                }
                self.previous_mode();
            }
        }
    }

    pub(crate) fn searchbox(&self) -> String {
        self.searchbox.lines().first().cloned().unwrap_or_default()
    }

    pub(crate) fn searchbox_widget(&self) -> impl ratatui::widgets::Widget + '_ {
        self.searchbox.widget()
    }

    pub(crate) fn searchbox_input(&mut self, key_event: impl Into<tui_textarea::Input>) {
        self.searchbox.input(key_event);
    }

    pub(crate) fn tagedit(&self) -> String {
        self.tagedit.lines().first().cloned().unwrap_or_default()
    }

    pub(crate) fn tagedit_widget(&self) -> impl ratatui::widgets::Widget + '_ {
        self.tagedit.widget()
    }

    pub(crate) fn tagedit_input(&mut self, key_event: impl Into<tui_textarea::Input>) {
        self.tagedit.input(key_event);
    }

    pub(crate) fn commandbox(&self) -> String {
        self.commandbox.lines().first().cloned().unwrap_or_default()
    }

    pub(crate) fn commandbox_widget(&self) -> impl ratatui::widgets::Widget + '_ {
        self.commandbox.widget()
    }

    pub(crate) fn commandbox_input(&mut self, key_event: impl Into<tui_textarea::Input>) {
        self.commandbox.input(key_event);
    }

    pub fn execute_command(&mut self) {
        let cmd = self.commandbox();
        match cmd.as_str() {
            "q" => self.quit(),
            "w" => {
                self.write();
            }
            "wq" => {
                self.write();
                self.quit();
            }
            "x" => {
                self.write();
                self.quit();
            }
            _ => {
                self.status = format!("Unknown command: {cmd:?}");
                self.previous_mode();
            }
        }
    }

    /// Switch to the mode that the app was in previously, e.g. pop the top of the mode stack
    pub fn previous_mode(&mut self) {
        self.mode_stack.pop_back();
    }

    fn write_tags_of_next_track(&mut self) -> WritingStatus {
        for track in &mut self.tracks.items {
            if track.changed {
                let result = track.write();
                match result {
                    Ok(_) => {
                        self.status = format!("Wrote {:?}", track.path);
                    }
                    Err(e) => {
                        self.status = format!("Error writing tags for {:?}: {e:?}", track.path)
                    }
                }
                return WritingStatus::InProgress;
            }
        }
        self.status = "Finished writing tracks".to_string();
        WritingStatus::Finished
    }

    pub fn status_bar_style(&self) -> Style {
        if !self.tracks_scan_finished || self.current_mode() == &Mode::WritingTags {
            Style::default().black().on_light_yellow()
        } else {
            Style::default().black().on_light_blue()
        }
    }

    /// Get the value of the currently selected tag for the currently selected track, or the nearest track found, searching backwards.
    ///
    /// Search order is like this:
    ///
    /// If the current mode is not TagSelect or TagEdit, return None.
    ///
    /// If there is no currently selected track, return None.
    ///
    /// Initialize an offset at 0.
    ///
    /// Iterate until every track is checked.
    ///
    /// Check the track before the selected one, at the offset. If the selected
    /// tag matches, return it.
    ///
    /// Check the track after the selected one, at the offset. If the selected
    /// tag matches, return it.
    ///
    /// If every track is checked, return None.
    ///
    /// If there are still tracks to check, increment the offset and repeat.
    ///
    ///
    /// This algorithm ensures that the visually closest selected tag value is
    /// returned, with a bias towards preferring tracks further up in the list.
    ///
    /// This is wanted because selecting a tag will move the cursor down the
    /// list.
    pub fn get_current_selected_tag_value(&self) -> Option<String> {
        let tag = match self.current_mode() {
            Mode::TagSelect(tag) => tag,
            Mode::TagEdit(tag) => tag,
            _ => return None,
        };
        if let Some(selected_track_idx) = self.tracks.state.selected() {
            let mut offset = 0;
            loop {
                // Check if track before selected matches
                if let Some(track) = self
                    .tracks
                    .items
                    .get(selected_track_idx.saturating_sub(offset))
                {
                    if track.selected_tag == Some(*tag) {
                        return Some(track.get_tag_value(*tag).to_string());
                    }
                }
                // Check if track after selected matches
                if let Some(track) = self.tracks.items.get(selected_track_idx + offset) {
                    if track.selected_tag == Some(*tag) {
                        return Some(track.get_tag_value(*tag).to_string());
                    }
                }

                // We have checked every track when:
                // - we have checked every track after the selected one
                // - we have checked every track before the selected one
                if selected_track_idx + offset > self.tracks.items.len()
                    && selected_track_idx.saturating_sub(offset) == 0
                {
                    return None;
                }

                // No match found, but we still have tracks to check, so
                // check a bit farther away from selected track
                offset += 1;
            }
        } else {
            // No track selected, return None
            //
            // This shouldn't happen, since to select a tag you need to select a track
            // and you can't unselect tracks
            None
        }
    }

    /// Clear all selected tags for all tracks
    pub fn clear_selected_tags(&mut self) {
        for track in &mut self.tracks.items {
            track.selected_tag = None;
        }
    }
}

pub enum SearchDirection {
    Forwards,
    Backwards,
}

enum WritingStatus {
    InProgress,
    Finished,
}
