// Copyright 2024 Khaïs COLIN <khais.colin@gmail.com>. All rights reserved.
//
// This file is part of Musicalasks.
//
// Musicalasks is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
//
// Musicalasks is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Musicalasks. If not, see <https://www.gnu.org/licenses/>. 
use crossterm::event::{KeyCode, KeyModifiers};

use crate::{app::App, app::Mode, event::Event};

pub fn update(app: &mut App, event: Event) {
    app.update();

    match app.current_mode() {
        Mode::Navigation => match event {
            Event::Tick => {}
            Event::Key(key_event) => match key_event.code {
                KeyCode::Esc | KeyCode::Char('q') => app.quit(),
                KeyCode::Char('c') if key_event.modifiers.contains(KeyModifiers::CONTROL) => {
                    app.quit()
                }
                KeyCode::Char('j') | KeyCode::Down => app.tracks.next(),
                KeyCode::Char('k') | KeyCode::Up => app.tracks.previous(),
                KeyCode::Char('g') => app.tracks.select_first(),
                KeyCode::Char('G') => app.tracks.select_last(),
                KeyCode::Char('/') => app.search_edit(),
                KeyCode::Char(':') => app.command_edit(),
                KeyCode::Char('n') => app.search_next(),
                KeyCode::Char('N') => app.search_previous(),
                KeyCode::Char('t') => app.title_edit(),
                KeyCode::Char('l') => app.album_tag_select(),
                KeyCode::PageUp => app.tracks.pageup(),
                KeyCode::PageDown => app.tracks.pagedown(),
                _ => {}
            },
            Event::Mouse(mouse_event) => match mouse_event.kind {
                crossterm::event::MouseEventKind::ScrollUp => app.tracks.previous(),
                crossterm::event::MouseEventKind::ScrollDown => app.tracks.next(),
                _ => {}
            },
            Event::Resize(_, _) => {}
        },
        Mode::TagSelect(_) => match event {
            Event::Tick => {}
            Event::Key(key_event) => match key_event.code {
                KeyCode::Esc | KeyCode::Char('q') => {
                    app.clear_selected_tags();
                    app.previous_mode()
                }
                KeyCode::Char('c') if key_event.modifiers.contains(KeyModifiers::CONTROL) => {
                    app.quit()
                }
                KeyCode::Char('j') | KeyCode::Down => app.tracks.next(),
                KeyCode::Char('k') | KeyCode::Up => app.tracks.previous(),
                KeyCode::Char('g') => app.tracks.select_first(),
                KeyCode::Char('G') => app.tracks.select_last(),
                KeyCode::Char('/') => app.search_edit(),
                KeyCode::Char(':') => app.command_edit(),
                KeyCode::Char('n') => app.search_next(),
                KeyCode::Char('N') => app.search_previous(),
                KeyCode::Char('t') => app.title_edit(),
                KeyCode::Char('l') | KeyCode::Char(' ') => app.album_tag_select(),
                KeyCode::PageUp => app.tracks.pageup(),
                KeyCode::PageDown => app.tracks.pagedown(),
                KeyCode::Enter => app.tag_edit_start(),
                _ => {}
            },
            Event::Mouse(mouse_event) => match mouse_event.kind {
                crossterm::event::MouseEventKind::ScrollUp => app.tracks.previous(),
                crossterm::event::MouseEventKind::ScrollDown => app.tracks.next(),
                _ => {}
            },
            Event::Resize(_, _) => {}
        },
        Mode::Command => match event {
            Event::Tick => {}
            Event::Key(key_event) => match key_event.code {
                KeyCode::Esc => app.previous_mode(),
                KeyCode::Char('c') if key_event.modifiers.contains(KeyModifiers::CONTROL) => {
                    app.quit()
                }
                KeyCode::PageUp => app.tracks.pageup(),
                KeyCode::PageDown => app.tracks.pagedown(),
                KeyCode::Char('m') if key_event.modifiers.contains(KeyModifiers::CONTROL) => {
                    app.execute_command()
                }
                KeyCode::Enter => app.execute_command(),
                KeyCode::Backspace if app.commandbox().is_empty() => app.navigate(),
                _ => {
                    app.commandbox_input(key_event);
                }
            },
            Event::Mouse(mouse_event) => match mouse_event.kind {
                crossterm::event::MouseEventKind::ScrollUp => app.tracks.previous(),
                crossterm::event::MouseEventKind::ScrollDown => app.tracks.next(),
                _ => {}
            },
            Event::Resize(_, _) => {}
        },
        Mode::SearchEdit => match event {
            Event::Tick => {}
            Event::Key(key_event) => match key_event.code {
                KeyCode::Esc => app.previous_mode(),
                KeyCode::Char('c') if key_event.modifiers.contains(KeyModifiers::CONTROL) => {
                    app.quit()
                }
                KeyCode::PageUp => app.tracks.pageup(),
                KeyCode::PageDown => app.tracks.pagedown(),
                KeyCode::Char('m') if key_event.modifiers.contains(KeyModifiers::CONTROL) => {
                    app.navigate()
                }
                KeyCode::Enter => app.previous_mode(),
                KeyCode::Backspace if app.searchbox().is_empty() => app.navigate(),
                _ => {
                    app.searchbox_input(key_event);
                }
            },
            Event::Mouse(mouse_event) => match mouse_event.kind {
                crossterm::event::MouseEventKind::ScrollUp => app.tracks.previous(),
                crossterm::event::MouseEventKind::ScrollDown => app.tracks.next(),
                _ => {}
            },
            Event::Resize(_, _) => {}
        },
        Mode::TitleEdit => match event {
            Event::Tick => {}
            Event::Key(key_event) => match key_event.code {
                KeyCode::Esc => app.previous_mode(),
                KeyCode::Char('c') if key_event.modifiers.contains(KeyModifiers::CONTROL) => {
                    app.quit()
                }
                KeyCode::Char('m') if key_event.modifiers.contains(KeyModifiers::CONTROL) => {
                    app.commit_tag_edit()
                }
                KeyCode::Enter => app.commit_tag_edit(),
                _ => {
                    app.tagedit_input(key_event);
                }
            },
            Event::Mouse(_) => {}
            Event::Resize(_, _) => {}
        },
        Mode::WritingTags => {}
        Mode::TagEdit(_) => match event {
            Event::Tick => {}
            Event::Key(key_event) => match key_event.code {
                KeyCode::Esc => app.previous_mode(),
                KeyCode::Char('c') if key_event.modifiers.contains(KeyModifiers::CONTROL) => {
                    app.quit()
                }
                KeyCode::Char('m') if key_event.modifiers.contains(KeyModifiers::CONTROL) => {
                    app.commit_tag_edit()
                }
                KeyCode::Enter => app.commit_tag_edit(),
                _ => {
                    app.tagedit_input(key_event);
                }
            },
            Event::Mouse(_) => {}
            Event::Resize(_, _) => {}
        },
    }
}
