# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- **status** - Show a status line with number of tracks loaded into memory and the artist and title of the currently selected track.
- **track list** - Add ability to select a track. It is highlighted in the UI.
- **status** - Show a path line with the path of the selected track.
- **cli** - Accept multiple paths for scanning.
- **scan** - Scanning now occurs on a separate thread with live UI updates.
- **keybinds** - PageUp, PageDown, Top, Bottom.
- **minibuffer** - Added minibuffer used for searching, which uses the excellent `tui_textarea` library to handle all editor functions.
- **search** - Added search. It is case insensitive, and requires that every word matches in a track. You can navigate forwards and backwards.
- **minibuffer** - Display current status message in minibuffer.
- **edit** - Add ability to edit title of the current track.
- **edit** - Add ability to edit album of selected tracks.
- **select** - Add ability to select the album tag of tracks.
- **command** - You can now enter commands in the minibuffer using ':', like in vi.
- **command** - New command 'q'. Quits the program.
- **command** - New command 'w'. Writes changes to file. Saving tags occurs in main thread, but progress is given to the user.
- **command** - New command 'wq'. Writes changes to file, and quits program.
- **command** - New command 'x'. Writes changes to file, and quits program.
- **status** - Show a short mnemonic for the current mode.
- **status** - If the app is currently performing actions in the background, such as loading track information or saving tags, this is indicated by an orange color of the status bar.

### Changed
- **track sorting** - Tracks are now intelligent grouped by album artist, date, album, disc number, track number, title, filename.
- **track list** - Instead of being blue, the path is now dimmed to better work with the color scheme.

### Fixed
- **edit** - Only advance to next track if edit is commited, not if cancelled.
- **sort** - Part of sorting was based on file timestamp instead of year tag.
