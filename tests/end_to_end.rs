// Copyright 2024 Khaïs COLIN <khais.colin@gmail.com>. All rights reserved.
//
// This file is part of Musicalasks.
//
// Musicalasks is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.
//
// Musicalasks is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Musicalasks. If not, see <https://www.gnu.org/licenses/>.
use std::path::PathBuf;

use color_eyre::Result;

use musicalasks::{app::App, event::Event, update::update};

fn test_assets_dir() -> PathBuf {
    let mut d = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    d.push("resources/test/Kevin MacLeod");
    d
}

#[test]
fn test_scanning_works() -> Result<()> {
    let paths: Vec<PathBuf> = vec![test_assets_dir()];
    let mut app = App::new(paths)?;

    while !app.tracks_scan_finished {
        std::thread::sleep(std::time::Duration::from_millis(50));
        update(&mut app, Event::Tick);
    }

    let reference_items: Vec<&str> = vec![
        "8bit Dungeon Boss",
        "Adventure Meme",
        "Alchemists Tower",
        "Amazing Plan",
        "Arcadia",
        "Bit Quest",
        "Call to Adventure",
        "Fig Leaf Times Two",
        "Gaslamp Funworks",
        "Industrial Cinematic",
        "Industrial Revolution",
        "Iron Horse",
        "Itty Bitty 8 Bit",
        "Local Forecast - Elevator",
        "Look Busy",
        "Lost Time",
        "Mechanolith",
        "Minstrel Guild",
        "New Hero in Town",
        "Professor and the Plant",
        "Run Amok",
        "Space Fighter Loop",
        "Spellbound",
        "That’s a Wrap",
        "The Builder",
        "The Cannery",
        "The Complex",
        "The Entertainer",
        "The Show Must Be Go",
        "Unholy Knight",
        "Unity",
    ];

    assert_eq!(app.tracks.items.len(), 31);
    let track_titles = app
        .tracks
        .items
        .iter()
        .map(|x| x.tags.title().expect("All tracks in test data have titles"))
        .collect::<Vec<&str>>();
    assert_eq!(track_titles, reference_items);
    assert!(app
        .tracks
        .items
        .iter()
        .all(|x| x.tags.artist() == Some("Kevin MacLeod")));
    assert!(app
        .tracks
        .items
        .iter()
        .all(|x| x.tags.album_title() == Some("Royalty Free")));

    Ok(())
}

#[test]
fn test_no_paths_scanned_lead_to_zero_files() -> Result<()> {
    let paths: Vec<PathBuf> = vec![];
    let mut app = App::new(paths)?;

    while !app.tracks_scan_finished {
        std::thread::sleep(std::time::Duration::from_millis(50));
        update(&mut app, Event::Tick);
    }

    assert!(app.tracks.items.is_empty());

    Ok(())
}
