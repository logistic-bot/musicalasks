# Musicalasks

**Musicalasks** is a TUI application for bulk-editing tags of music files.

It is designed for fast operation when editing the tags of a single album.

![screenshot](screenshot.png)

## Documentation

### Installation

Clone the repository, and install the application. This will compile it as well.

```sh
cargo install --path .
```

## Licenses

Musicalasks is licensed under the GNU GPL version 3.

Test music files:

"8bit Dungeon Boss", "Adventure Meme", "Alchemists Tower", "Amazing Plan", "Arcadia", "Bit Quest", "Call to Adventure", "Fig Leaf Times Two", "Gaslamp Funworks", "Industrial Cinematic", "Industrial Revolution", "Iron Horse", "Itty Bitty 8 Bit", "Local Forecast - Elevator", "Look Busy", "Mechanolith", "Minstrel Guild", "New Hero in Town", "Professor and the Plant", "Run Amok", "Space Fighter Loop", "Spellbound", "That's a Wrap", "The Builder", "The Cannery", "The Complex", "The Entertainer", "The Show Must Be Go", "Unholy Knight", "Unity"</br>
Kevin MacLeod (incompetech.com)</br>
Licensed under Creative Commons: By Attribution 3.0</br>
http://creativecommons.org/licenses/by/3.0/

## Contact

For any questions, comments or inquiries, please contact me by email: [khais.colin@gmail.com](mailto:khais.colin@gmail.com).
